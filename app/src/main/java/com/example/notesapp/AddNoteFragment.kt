package com.example.notesapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.notesapp.data.Note
import com.example.notesapp.databinding.FragmentAddNoteBinding
import com.google.firebase.analytics.FirebaseAnalytics
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class AddNoteFragment : Fragment() {

    private lateinit var binding: FragmentAddNoteBinding
    private val mainViewModel: MainViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_add_note, container, false
        )
        binding.viewModel = mainViewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            viewModel = mainViewModel
        }

        binding.buttonSaveNote.setOnClickListener {
            val noteName = binding.noteNameText.text.toString()
            val noteContent = binding.noteContentText.text.toString()

            val currentDate = Date()
            val dateFormat = SimpleDateFormat("MMM d, yyyy h:mm:ssa", Locale.getDefault())
            val noteDate = dateFormat.format(currentDate)

            val newNote = Note(name = noteName, content = noteContent, date = noteDate)
            mainViewModel.addNote(newNote)

            mainViewModel.setNoteData(newNote)
            mainViewModel.setNoteName(noteName)
            mainViewModel.setNoteContent(noteContent)

            val bundle = Bundle()
            bundle.putLong(FirebaseAnalytics.Param.ITEM_ID, newNote.id)
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, newNote.name)
            FirebaseAnalytics.getInstance(requireContext()).logEvent("add_note", bundle)
            findNavController().navigate(R.id.mainFragment)
        }
    }
}
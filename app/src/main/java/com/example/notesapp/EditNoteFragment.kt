package com.example.notesapp

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.notesapp.data.Note
import com.example.notesapp.databinding.FragmentAddNoteBinding
import com.example.notesapp.databinding.FragmentEditNoteBinding

class EditNoteFragment : Fragment() {

    private lateinit var binding: FragmentEditNoteBinding
    private val mainViewModel: MainViewModel by activityViewModels()
    private var selectedNote: Note? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_edit_note, container, false
        )
        binding.viewModel = mainViewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            viewModel = mainViewModel
        }

        binding.noteNameText.setText(mainViewModel.noteName.value)
        binding.noteContentText.setText(mainViewModel.noteContent.value)

        mainViewModel.noteData.observe(viewLifecycleOwner) { note ->
            selectedNote = note
            Log.d("EditNoteFragment", "Selected Note: $note")

        }
        binding.buttonEditNote.setOnClickListener {
                mainViewModel.editNote(Note(
                    id = mainViewModel.noteData.value?.id!!,
                    name = binding.noteNameText.text.toString(),
                    content = binding.noteContentText.text.toString(),
                    date = mainViewModel.noteData.value?.date
                ))
            findNavController().navigate(R.id.mainFragment)
        }
    }

}
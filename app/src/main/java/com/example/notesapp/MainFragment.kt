package com.example.notesapp

import  android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.notesapp.data.Note
import com.example.notesapp.databinding.FragmentMainBinding
import kotlinx.coroutines.launch


class MainFragment : Fragment(), NotesAdapter.OnItemClickListener {

    private lateinit var binding: FragmentMainBinding
    private val noteList = mutableListOf<Note>()
    private val mainViewModel: MainViewModel by activityViewModels{
        MainViewModelFactory(requireActivity())
    }
    private val adapter = NotesAdapter(this)


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_main, container, false
        )
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.viewModel = mainViewModel

        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        binding.recyclerView.adapter = adapter

        lifecycleScope.launch {
            mainViewModel.allNotes.collect {
                adapter.updateData(it)
                adapter.notifyDataSetChanged()
            }
        }

        binding.buttonCreateNote.setOnClickListener {
            findNavController().navigate(R.id.addNoteFragment)
        }
    }

    override fun onItemClick(note: Note) {
        mainViewModel.setNoteData(note)
        note.name?.let { mainViewModel.setNoteName(it) }
        note.content?.let { mainViewModel.setNoteContent(it) }
        findNavController().navigate(R.id.viewNoteFragment)
    }

    override fun onResume() {
        super.onResume()
        mainViewModel.getAll()
    }

}

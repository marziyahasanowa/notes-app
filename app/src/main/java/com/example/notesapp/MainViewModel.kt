package com.example.notesapp

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.notesapp.data.Note
import com.example.notesapp.data.NoteDao
import com.example.notesapp.data.NoteDatabase
import com.example.notesapp.data.NoteRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class MainViewModel(context: Context,
    private val dao: NoteDao = NoteDatabase.getDatabase(context).noteDao(),
    private val repository: NoteRepository = NoteRepository(dao)
) : ViewModel() {


    private val _noteData = MutableLiveData<Note>()
    val noteData: LiveData<Note> get() = _noteData

    fun setNoteData(note: Note) {
        _noteData.value = note
        Log.d("MainViewModel", "Note data set: $note")
    }

    private val _noteName = MutableLiveData<String>()
    val noteName: LiveData<String> get() = _noteName

    fun setNoteName(name: String) {
        _noteName.value = name
        Log.d("MainViewModel", "Note name set: $name")
    }

    private val _noteContent = MutableLiveData<String>()
    val noteContent: LiveData<String> get() = _noteContent

    fun setNoteContent(content: String) {
        _noteContent.value = content
        Log.d("MainViewModel", "Note content set: $content")
    }

    val _allNotes: MutableStateFlow<List<Note>> = MutableStateFlow(emptyList())
    val allNotes: StateFlow<List<Note>> = _allNotes

    fun getAll() {
        viewModelScope.launch {
            repository.getAllNotes().collect {
                _allNotes.value = it
            }
        }
    }


    fun addNote(note: Note) = viewModelScope.launch {
        repository.insertNote(note)
    }

    fun editNote(note: Note) = viewModelScope.launch {
        repository.updateNote(note)
    }

    fun deleteNote(note: Note) = viewModelScope.launch {
        repository.deleteNote(note)
    }

}

class MainViewModelFactory(private val context: Context) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
            @Suppress("UNCHECKED_CAST")
            return MainViewModel(context) as T
    }
}
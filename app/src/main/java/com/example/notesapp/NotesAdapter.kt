package com.example.notesapp

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.notesapp.data.Note
import com.example.notesapp.databinding.CardviewNoteListBinding

class NotesAdapter(
    private val listener: OnItemClickListener,
) : RecyclerView.Adapter<NotesViewHolder>() {

    private var notesList: List<Note> = emptyList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotesViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = CardviewNoteListBinding.inflate(inflater, parent, false)
        return NotesViewHolder(binding, listener)
    }

    override fun getItemCount(): Int {
        return notesList.size
    }

    override fun onBindViewHolder(holder: NotesViewHolder, position: Int) {
        holder.bind(notesList[position])
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateData(newNotesList: List<Note>) {
        notesList = newNotesList
    }

    interface OnItemClickListener{
        fun onItemClick(note: Note){

        }
    }
}

class NotesViewHolder(
    private val binding: CardviewNoteListBinding,
    private val listener: NotesAdapter.OnItemClickListener,
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(note: Note) {
//        binding.noteItemName.text = note.name
//        binding.noteDate.text = note.date

        binding.model = note
        binding.itemCard.setOnClickListener {
            listener.onItemClick(note)
        }
    }

}






package com.example.notesapp

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.notesapp.data.Note
import com.example.notesapp.databinding.FragmentViewNoteBinding
import com.google.firebase.analytics.FirebaseAnalytics

class ViewNoteFragment : Fragment() {

    private lateinit var binding: FragmentViewNoteBinding
    private var selectedNote: Note? = null
    private val mainViewModel: MainViewModel by activityViewModels()


    private var noteName:String? = null
    private var noteContent:String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_view_note, container, false
        )
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            viewModel = mainViewModel
        }


        mainViewModel.noteData.observe(viewLifecycleOwner) { note ->
            selectedNote = note
            Log.d("ViewNoteFragment", "Selected Note: $note")

            val bundle = Bundle()
            bundle.putLong(FirebaseAnalytics.Param.ITEM_ID, note.id)
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, note.name)
            FirebaseAnalytics.getInstance(requireContext()).logEvent("view_note", bundle)

        }
        binding.buttonDelete.setOnClickListener {
            if (selectedNote != null) {
                mainViewModel.deleteNote(selectedNote!!)
            }

            val bundle = Bundle()
            bundle.putLong(FirebaseAnalytics.Param.ITEM_ID, selectedNote!!.id)
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, selectedNote!!.name)
            FirebaseAnalytics.getInstance(requireContext()).logEvent("remove_note", bundle)
            findNavController().navigate(R.id.mainFragment)
        }

        binding.buttonEdit.setOnClickListener {
            findNavController().navigate(R.id.editNoteFragment)
        }
    }

}
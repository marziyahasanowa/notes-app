package com.example.notesapp.data

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class NoteRepository(
    private val noteDao: NoteDao
) {

    suspend fun getAllNotes() = withContext(Dispatchers.IO) {
        noteDao.getAll()
    }

    suspend fun insertNote(note: Note) = withContext(Dispatchers.IO) {
        noteDao.insertNote(note)
    }


    suspend fun updateNote(note: Note) = withContext(Dispatchers.IO) {
        noteDao.updateNote(note)
    }


    suspend fun deleteNote (note: Note) = withContext(Dispatchers.IO) {
        noteDao.deleteNote(note)
    }


}